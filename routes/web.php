<?php

use App\Http\Controllers\AdController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Интерфейс для добавления и вывода объявлений

Route::get('/', function () {
    return redirect()->route('ads.index');
});

Route::resource('ads', AdController::class)->only([
    'index', 'store', 'show'
]);
