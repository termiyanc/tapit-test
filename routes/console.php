<?php

use App\Jobs\UploadAdsImages;
use App\Models\Ad;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('ads:show {id?}', function ($id = null) {
    $tableHeaders = [
        'Id',
        'Название',
        'Описание',
        'Цена',
        'Дата добавления',
        'Изображения'
    ];

    $adsQuery = Ad::select(['id', 'name', 'description', 'price', 'created_at', 'images']);

    $this->table($tableHeaders, $id ? $adsQuery->whereId($id)->get() : $adsQuery->get());
})->purpose('Вывести список объявлений, или вывести одно объявление по переданному после команды id');

Artisan::command('ads:uploadImages', function () {
    UploadAdsImages::dispatch();
})->purpose('Инициировать загрузку изображений по пользовтаельским ссылкам в объявлениях');
