<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Ad extends Model
{
    protected $fillable = [
        'name',
        'description',
        'price',
        'images'
    ];

    protected $casts = [
        'images' => 'array',
        'images_uploaded' => 'array'
    ];

    /**
     * Первая пользовательская ссылка на изображение
     * @todo: не отдавать несуществующее изображение, или возвращать загруженное по ссылке изображение
     * @return string
     */
    public function getImageAttribute()
    {
        return Arr::first($this->images);
    }
}
