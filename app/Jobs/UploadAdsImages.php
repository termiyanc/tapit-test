<?php

namespace App\Jobs;

use App\Models\Ad;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class UploadAdsImages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Загрузка на сервер изображений объявлений
     *
     * @todo В images_uploaded пишется null
     * @return void
     */
    public function handle()
    {
        Ad::select('images')->whereNull('images_uploaded')->each(function ($ad) {
            $ad->images_uploaded = array_map(function ($imageUrl) {
                try {
                    // Загрузка содержимого предполагаемого изображения
                    if ($imageContents = file_get_contents($imageUrl)) {
                        // Проверка полученного содержимого на соответствие MIME-типу изображения
                        if (mb_strpos((new \finfo(FILEINFO_MIME_TYPE))->buffer($imageContents), 'image') !== false) {
                            $filename = basename($imageUrl);
                            // Сохранение подтвержденного изображения
                            if (Storage::put($filename, $imageContents)) {
                                return $filename;
                            }
                        }
                    }
                } catch (\Exception $e) {
                    // Если по какой-то причине изображение загрузить не удалось, в массив пишется false
                    return false;
                }

            }, $ad->images);

            $ad->save();
        });
    }
}
