<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Валидация формы добавления объявления
 *
 * @todo Показывать пользователю ошибку валидации
 * @package App\Http\Requests
 */
class StoreAd extends FormRequest
{
    /**
     * Максимальное количество ссылок на изображение у объявления
     */
    public const IMAGES_MAX_COUNT = 3;

    /**
     * @return bool
     */
    public function authorize()
    {
        return true; // Авторизацию проверять не нужно - мнимый пользователь всегда авторизован
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'images' => 'required|min:1|max:' . static::IMAGES_MAX_COUNT, // todo: проверять переданные ссылки на существование по ним изображений
            'description' => 'required|max:1000',
            'name' => 'required|max:200',
            'price' => 'required|min:1'
        ];
    }
}
