<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAd;
use App\Models\Ad;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AdController extends Controller
{
    /**
     * Вывод списка объявлений с пагинацией и сортировкой
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $adsQuery = Ad::query();

        if ($orderBy = $request->get('orderBy')) {
            $adsQuery = $adsQuery->orderBy($orderBy, $request->get('orderDirection', 'asc'));
        }

        return view('ad.index', [
            'ads' => $adsQuery->paginate(10), ['*'], 'page', $request->get('page'),
            'imagesMaxCount' => StoreAd::IMAGES_MAX_COUNT
        ]);
    }

    /**
     * Добавление нового объявления
     *
     * @param StoreAd $request
     * @return Response
     */
    public function store(StoreAd $request)
    {
        Ad::create($request->validated());

        return redirect()->route('ads.index');
    }

    /**
     * Вывод отдельного объявления
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return Ad::findOrFail($id);
    }
}
