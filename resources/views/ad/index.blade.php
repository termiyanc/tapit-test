<!doctype html>
<html>
<head>
    <title>Доска объявлений</title>
    <style type="text/css">
        body {
            width: 800px;
        }

        .ads-order-by a {
            display: block;
        }

        .ads-list {
            padding: 10px 0;
            margin: 20px 0;
            border-top: 1px solid #000;
            border-bottom: 1px solid #000;
        }

        .ads-item:not(:last-child) {
            border-bottom: 1px solid #ccc;
        }

        .ads-store-form {
            width: 600px;
        }

        .ads-store-form input,
        .ads-store-form textarea {
            display: block;
            margin-bottom: 10px;
        }

        .ads-store-form textarea {
            width: 100%;
        }
    </style>
</head>
<body style="width: 800px;">
@if ($ads->count())
    <div class="ads-order-by">
        <span>Сортировать по:</span>
        <a href="?orderBy=price">цене по возрастанию</a>
        <a href="?orderBy=price&orderDirection=desc">цене по убыванию</a>
        <a href="?orderBy=created_at">дате добавления по возрастанию</a>
        <a href="?orderBy=created_at&orderDirection=desc">дате добавления по убыванию</a>
    </div>
    <div class="ads-list">
        @foreach ($ads as $ad)
            <div class="ads-item">
                <p>{{ $ad->name }}</p>
                <p>{{ $ad->description }}</p>
                <p style="font-style: italic">{{ $ad->price }}</p>
                <p><a href="{{ $ad->image }}">{{ $ad->image }}</a></p>
            </div>
        @endforeach
    </div>
    {{ $ads->links() }}
@else
    <p>Объявления отсутствуют.</p>
@endif

<form class="ads-store-form" method="POST" action="{{ route('ads.store') }}">
    @csrf
    <label>
        <span>Название:</span>
        <input name="name">
    </label>
    <label>
        <span>Описание:</span>
        <textarea name="description"></textarea>
    </label>
    <label>
        <span>Цена:</span>
        <input name="price">
    </label>
    <label>
        <span>Ссылки на изображения:</span>
        @for ($i = 1; $i <= $imagesMaxCount; $i++)
            <input name="images[]">
        @endfor
    </label>
    <input type="submit" value="Отправить">
</form>
</body>
</html>
